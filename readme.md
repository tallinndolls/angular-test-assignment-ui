# Test assignment:

This is a test assigment to test your skills with Angular, Node.js and MySQL.  We will assess your system thinking, 
coding style, and ability to work with the tools that we use in our projects.

-----------------------------------------

# Feature: Product creation and filtering

## User story

As a user, I want to be able to add new products and search products based on creation date range. I want 
to fill in product name and price to create a new product. I want to see a table of existing products with name,
price and creation date columns. I want to select date range to filter the product list by creation date. I expect the
UI and filtering to work with up to 10 000 products.

## Implementation

- Create a single page app that stores and fetches the products over REST API.
- Create REST API.
- Use following tools for backend REST API: Node + Express + MySQL + Jest
- Use following tools for frontend: Angular 6 + Angular Material + Jest
- Use Angular material for the UI. Add product creation form and product list with filter.
- Add as many unit tests and integration tests as you see fit.
- Use code style and best practices that you see fit.
- Addition to sending the source code, record a video where you show how the app works. 
Go through the steps described in the user story.

## Definition of done

- Features completed
- Unit tests / integration tests added and passing
- New Bitbucket/Github repo created
- Source code pushed to repo and repo link sent over email
- Video showing the user story recorded. Video link sent over email.
